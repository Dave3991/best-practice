#!/usr/bin/env bash
# shebang/or bang line - ensures that Bash will be used to interpret the script, even if it is executed under another shell.

set -e # if any command end with exit non-zero code -> crash
set -u # if we will use undefined variable -> crash
set -o pipefail # if any pipe/command fail -> crash ... normally will bash check only ouput of last command e.g  command1 | commmand2 | command 3 ... normally will check only command3 exit code
